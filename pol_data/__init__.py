import pol_data.midas_pol.file_reader as file_reader
import datetime

class PolRunInfo:
    """
    Configuration of the POL experiment for a given midas run.

    Members:

    * run_number (int) - Midas run number
    * run_start_time (int) - Start time of the run (UNIX timestamp)

    * cycles_per_voltage_step (int) - Number of repetitions at each DAC voltage before continuing
    * hist_dwell_time_ms (number) - Length of each time bin in the histogram, in ms
    * histo_info (dict of {str: {"human_name": str, "scaler_chan": int}}) - Configuration
        of the midas data banks for each scaler input. Key is bank name.
        E.g. "HIS1": {"human_name": "PMT", "scaler_chan": 21}
    * ppg_program (dict) - The PPG program that was run
    * num_x_steps (int) - Number of steps in scan (data is taken at N+1 points)
    * num_y_steps (int) - Number of steps in 2D scan (not currently used by POL)
    * scan_rio_x (list of str) - Which RIO channels were being scanned (e.g. "RIO channel 3"). 
        Empty string means not scanned.
    * scan_epics_x (list of str) - Which EPICS PVs were being scanned. Empty string means not scanned.
    """
    def __init__(self, odb):
        """
        This only supports files from the "new" DAQ, as the old DAQ contains
        characters that are no valid utf-8, and so cannot be parsed as JSON.

        Arguments:

        * odb (dict) - ODB dump as parsed from midas file
        """
        self.run_number = odb["Runinfo"]["Run number"]
        start_binary = odb["Runinfo"]["Start time binary"]
        self.run_start_time = int(start_binary, 16) if isinstance(start_binary, str) else int(start_binary)

        self.ppg_program = odb["Equipment"]["PPGCompiler"]["Programming"]

        try:
            self.cycles_per_voltage_step = odb["Equipment"]["PPGCompiler"]["Settings"]["Auto loop num iterations"]
        except:
            self.cycles_per_voltage_step = None
        
        try:
            self.hist_dwell_time_ms = self.ppg_program["del_dwell_time"]["time offset (ms)"]
        except:
            self.hist_dwell_time_ms = 1

        histo_settings = odb["Scanning"]["PolHistos"]["Settings"]
        self.histo_info = {}

        for i, bank_name in enumerate(histo_settings["Channel bank name"]):
            if bank_name != "":
                human_name = histo_settings["Channel human name"][i]
                if human_name == "":
                    human_name = "Channel %d" % (i+1)

                self.histo_info[bank_name] = {"human_name": human_name, "scaler_chan": i+1}

        x_enabled = odb["Scanning"]["Global"]["Settings"]["Enable X"]
        y_enabled = odb["Scanning"]["Global"]["Settings"]["Enable Y"]

        self.num_x_steps = odb["Scanning"]["Global"]["Settings"]["nX steps"] if x_enabled else 0
        self.num_y_steps = odb["Scanning"]["Global"]["Settings"]["nY steps"] if y_enabled else 0

        self.epics_laser_readback = []

        try:
            if odb["Scanning"]["EpicsLaserRead"]["Settings"]["Enable logging"]:
                self.epics_laser_readback = odb["Scanning"]["EpicsLaserRead"]["Computed"]["PV list"]
        except KeyError:
            pass

        epics_settings = odb["Scanning"]["Epics"]["Settings"]
        epics_dev_list = odb["Scanning"]["Epics"]["Definitions"]["Demand device"]

        if x_enabled:
            self.scan_rio_x = ["RIO channel %s" % chan if chan >= 0 else "" for chan in odb["Scanning"]["GalilRIOScan"]["Settings"]["X scan channels"]]
            self.scan_epics_x = [epics_dev_list[idx] if idx >= 0 else "" for idx in epics_settings["X devices"] ]
        else:
            self.scan_rio_x = None
            self.scan_epics_x = None

        if y_enabled:
            self.scan_rio_y = ["RIO channel %s" % chan if chan >= 0 else "" for chan in odb["Scanning"]["GalilRIOScan"]["Settings"]["Y scan channels"]]
            self.scan_epics_y = [epics_dev_list[idx] if idx >= 0 else "" for idx in epics_settings["Y devices"] ]
        else:
            self.scan_rio_y = None
            self.scan_epics_y = None
            
    def dump_to_screen(self):
        """
        Dump a summary of this run configuration to screen.
        """
        print("Run # %s, started at %s" % (self.run_number, datetime.datetime.fromtimestamp(self.run_start_time)))

        if self.num_x_steps > 1 and self.num_y_steps > 1:
            print("  2D scan over %d X steps and %d Y steps" % (self.num_x_steps, self.num_y_steps))
        elif self.num_x_steps > 1:
            print("  1D scan over %d X steps" % (self.num_x_steps))
        else:
            print("  No variables being scanned")

    def is_epics_scanned(self):
        if self.scan_epics_x is not None:
            for x in self.scan_epics_x:
                if x != "":
                    return True

        if self.scan_epics_y is not None:
            for y in self.scan_epics_y:
                if y != "":
                    return True
                    
        return False

    def get_all_scanned_var_names(self):
        """
        Get a list of all the variables (RIO/EPICS) that are being scanned in this run.

        Returns:

        * 2-tuple of (list of string, list of string), for 1D/2D scan.
        """
        x_keys = []
        y_keys = []

        if self.num_x_steps > 0:
            x_keys.extend([p for p in self.scan_rio_x if p != ""])
            x_keys.extend([p for p in self.scan_epics_x if p != ""])

        if self.num_y_steps > 0:
            y_keys.extend([p for p in self.scan_rio_y if p != ""])
            y_keys.extend([p for p in self.scan_epics_y if p != ""])

        return (x_keys, y_keys)

    
def scaler_chan_to_human_name(run_info, scaler_chan):
    """
    Get the "human name" for a scaler channel.

    Arguments:

    * run_info (`PolRunInfo` or None)
    * scaler_chan (int)

    Returns:
    
    * string.
    """
    info = get_histo_info(run_info)

    for details in info.values():
        if details["scaler_chan"] == scaler_chan:
            return details["human_name"]

    return "Scaler input %d" % scaler_chan

def get_histo_info(run_info):
    """
    Get the histogram/scaler configuration for a run, or a default
    configuration for older runs where run_info is None.

    Arguments:

    * run_info (`PolRunInfo` or None)

    Returns:

    * dict - see `PolRunInfo.histo_info` for details.
    """
    if run_info is not None:
        return run_info.histo_info
    else:
        return {
            "HIS0": {"human_name": "MCS0 (All Frequencies)", "scaler_chan": 21},
            "HIS1": {"human_name": "MCS1 (Frequency 1)", "scaler_chan": 22},
            "HIS2": {"human_name": "MCS2 (Frequency 2)", "scaler_chan": 23},
            "HIS3": {"human_name": "MCS3 (Frequency 3)", "scaler_chan": 24}
        }

class PolEvent:
    """
    Summary of an event from the Pol experiment.

    Members:

    * event_time (number) - UNIX timestamp
    * event_number (int) - midas serial number
    * hists (dict of {int: list of int}) - Data from each scaler channel.
        Key is scaler channel, value is the time-bin histogram.
    * hists_human (dict of {int: str}) - Human name for each scaler channel.
    * sums_dict (dict of {int: number}) - Sum of histogram for each scaler
        channel. Key is scaler channel.
    * rio_input_latest (list of number) - Latest voltage reading from first 4 RIO inputs.
    * rio_input_averages (list of number) - Average voltage reading from first 4 RIO inputs.
    * rio_scan_values (dict of {str: number}) - Value of each RIO DAC that
        is being scanned. Key is like "RIO channel 3", value is voltage for this event.
    * epics_scan_values (dict of {str: number}) - Value of each EPICS PV that
        is being scanned. Key is PV name, value is setting for this event.
    * loop_count (int) - Number of times we've scanned through all the 
        voltages/EPICS settings.
    * x_step (int) - Step in the scan.
    * y_step (int) - Step in the 2D scan (not used by POL yet).
    * cycle_count (int) - Number of PPG cycles completed.
    * cycle_info (list of number) - Low-level info from the CYCL bank
    * hisi_info (list of number) - Low-level info from the HISI bank
    * sums_list (list of number) - Low-level info from the HSUM bank
    * epics_readback (dict of {str: number}) - EPICS PV name and value
    * keysight_voltage (number) - Voltage reading from Keysight 34465A (in Volts)
    """
    def __init__(self, midas_event, run_info):
        """
        Arguments:

        * midas_event (`midas.event.MidasEvent`)
        * run_info (`PolRunInfo` or None)
        """
        expected_histo_names = get_histo_info(run_info).keys()

        if not midas_event.bank_exists("CYCL") or midas_event.header.event_id != 5:
            raise ValueError("Not the right kind of event for us")

        if midas_event.bank_exists("ETMS"):
            # 1 ms precision event time
            self.event_time = midas_event.banks["ETMS"].data[0] / 1000.
        else:
            # 1 second precision event time
            self.event_time = midas_event.header.timestamp
            
        self.event_number = midas_event.header.serial_number
        
        self.cycle_info = midas_event.banks["CYCL"].data
        self.hisi_info = midas_event.banks["HISI"].data

        self.epics_readback = {}
        self.hists = {}
        self.hists_human = {}

        histo_info = get_histo_info(run_info)
        
        for bank_name in expected_histo_names:
            if midas_event.bank_exists(bank_name):
                chan = histo_info[bank_name]["scaler_chan"]
                self.hists[chan] = midas_event.banks[bank_name].data
                self.hists_human[chan] = scaler_chan_to_human_name(run_info, chan)
                
        self.sums_list = midas_event.banks["HSUM"].data
        self.sums_dict = {}

        # Convert HSUM bank from simple list to dict keyed by scaler number
        chan_list = [h["scaler_chan"] for h in histo_info.values()]

        for idx, chan in enumerate(sorted(chan_list)):
            self.sums_dict[chan] = self.sums_list[idx]

        self.rio_scan_values = {}
        self.rio_input_latest = [-9999] * 4
        self.rio_input_average = [-9999] * 4
        self.epics_scan_values = {}
        self.scan_set = -9999
        self.scan_rdb = -9999
        self.keysight_voltage = -9999

        if midas_event.bank_exists("SCAN"):
            # New DAQ - supports Galil-RIO and EPICS scanning
            self.loop_count = midas_event.banks["SCAN"].data[0]
            self.x_step = midas_event.banks["SCAN"].data[1]
            self.y_step = midas_event.banks["SCAN"].data[2]
            self.cycle_count = midas_event.banks["SCAN"].data[5]

            # RIO inputs
            if midas_event.bank_exists("RIOL"):
                for chan, v in enumerate(midas_event.banks["RIOL"].data):
                    self.rio_input_latest[chan] = v

            if midas_event.bank_exists("RIOA"):
                for chan, v in enumerate(midas_event.banks["RIOA"].data):
                    self.rio_input_average[chan] = v

            # RIO outputs
            if midas_event.bank_exists("XRIO"):
                for i, chan in enumerate(run_info.scan_rio_x):
                    if chan != "" and chan is not None:
                        self.rio_scan_values[chan] = midas_event.banks["XRIO"].data[i]

            if midas_event.bank_exists("YRIO"):
                for i, chan in enumerate(run_info.scan_rio_y):
                    if chan != "" and chan is not None:
                        self.rio_scan_values[chan] = midas_event.banks["YRIO"].data[i]

            # EPICS demand settings
            if midas_event.bank_exists("XEPD"):
                for i, dev in enumerate(run_info.scan_epics_x):
                    if dev != "" and dev is not None:
                        self.epics_scan_values[dev] = midas_event.banks["XEPD"].data[i]

            if midas_event.bank_exists("YEPD"):
                for i, dev in enumerate(run_info.scan_epics_y):
                    if dev != "" and dev is not None:
                        self.epics_scan_values[dev] = midas_event.banks["YEPD"].data[i]
        else:
            # Old DAQ - only supports Galil-RIO scanning
            self.loop_count = midas_event.banks["CYCL"].data[4]
            self.x_step = midas_event.banks["CYCL"].data[7]
            self.y_step = -1
            self.cycle_count = midas_event.banks["CYCL"].data[0]
            self.rio_scan_values = {"RIO channel 0": midas_event.banks["CYCL"].data[8]}
            self.epics_scan_values = {}
            self.rio_input_latest = []
            self.rio_input_average = []

        # Both DAQs have the HISI bank for "simple" lookup of scan setting/readback.
        if midas_event.bank_exists("HISI"):
            self.scan_set = midas_event.banks["HISI"].data[2]
            self.scan_rdb = midas_event.banks["HISI"].data[3]

        # EPICS readback values
        if midas_event.bank_exists("LASR") and run_info.epics_laser_readback != []:
            for i in range(min(len(midas_event.banks["LASR"].data), len(run_info.epics_laser_readback))):
                self.epics_readback[run_info.epics_laser_readback[i]] = midas_event.banks["LASR"].data[i]

        if midas_event.bank_exists("KEYS"):
            self.keysight_voltage = midas_event.banks["KEYS"].data[0]

    def dump_to_screen(self):
        """
        Print a summary of this event to screen.
        """
        print("Event # %d at %s" % (self.event_number, datetime.datetime.fromtimestamp(self.event_time)))
        print("  Loop %d, scan step %d" % (self.loop_count, self.x_step))

        if len(self.rio_scan_values):
            print("  Galil-RIO scanning:")
            for k, v in self.rio_scan_values.items():
                print("    %s => %.4f" % (k, v))

        if len(self.rio_input_latest):
            print("  Galil-RIO input readings:")
            for chan, v in enumerate(self.rio_input_latest):
                if chan < len(self.rio_input_average):
                    print("    %d => %.4f (latest), %.4f (average)" % (chan, v, self.rio_input_average[chan]))
                else:
                    print("    %d => %.4f (latest)" % (chan, v))

        if len(self.epics_scan_values):
            print("  EPICS scanning:")
            for k, v in self.epics_scan_values.items():
                print("    %s => %.4f" % (k, v))

        if len(self.hists):
            print("  Histogram totals:")
            for chan, hist_data in self.hists.items():
                print("    Scaler chan %s => %s counts (%s)" % (chan, sum(hist_data), self.hists_human[chan]))

        if len(self.epics_readback):
            print("  EPICS readback:")
            for k, v in self.epics_readback.items():
                print("    %s => %.4f" % (k, v))

        if self.keysight_voltage != -9999:
            print("  Keysight voltage (V): %s" % self.keysight_voltage)

    def get_all_scanned_vars(self, runinfo):
        """
        Get the value of all variables being scanned in this run.

        Returns:

        * dict of {str: number}
        """
        scan_vals = self.rio_scan_values
        scan_vals.update(self.epics_scan_values)
        return scan_vals

class PolFile:
    """
    Class that provides easy access to POL-specific data
    """
    def __init__(self, file_path, raise_if_bad_odb=False):
        self.midas_file = file_reader.MidasFile(file_path)

        try:
            odb = self.midas_file.get_bor_odb_dump()
        except Exception as e:
            if raise_if_bad_odb:
                raise e
            else:
                odb = None

        try:
            self.run_info = PolRunInfo(odb.data)
        except:
            self.run_info = None

    def get_all_event_info(self):
        """
        Returns data for all events in a file as a list.
        This may be slow and memory-inefficient if you have a large number
        of events in the file.
        """
        return [ev for ev in self]

    def __next__(self):
        """
        Iterable interface for looping through events.
        """
        while True:
            midas_event = self.midas_file.read_next_event()

            if midas_event is None:
                # End of file - stop iterating
                raise StopIteration()

            # Try to convert to an PolEvent
            try:
                pol_event = PolEvent(midas_event, self.run_info)
            except ValueError:
                # Internal midas event; move on to next real event
                continue

            return pol_event

    next = __next__ # for Python 2

    def __iter__(self):
        """
        Iterable interface for looping through events.
        """
        return self
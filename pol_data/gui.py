import traceback
import numpy as np
import matplotlib.pyplot as plt
from copy import deepcopy
import tkinter.filedialog
import pol_data
import os.path
from tkinter import *

font = {'family': 'sans-serif', 'weight': 'normal', 'size': 16}
plt.rc('font', **font)
plt.rcParams['xtick.major.pad']='10'
plt.rcParams['ytick.major.pad']='10'

class simpleapp_tk(tkinter.Tk):
    def __init__(self,parent):
        tkinter.Tk.__init__(self,parent)
        self.parent = parent
        self.initialize()

    def initialize(self):
        self.cycl_buttons = {}
        self.grid()
        self.minsize(width=30, height=30)
        self.resizable(True,True)
        self.last_open_dir = None
        self.epics_scanned = False

        # Create Arrays for Title Information
        self.cyclnames = ["Not Used",
            "Scan Type Code",
            "Cycle Counter",
            "Supercycle Counter",
            "Cycles/SC Counter",
            "Sweep Counter",
            "Skipped Cycle Counter",
            "# Cycles Histogrammed",
            "Scan Increment Counter",
            "Scan Setting",
            "ADC[0] Input",
            "ADC[1] Input",
            "ADC[2] Input",
            "ADC[3] Input",
            "Averaged ADC[0] Input",
            "Averaged ADC[1] Input",
            "Averaged ADC[2] Input",
            "Averaged ADC[3] Input", # End of true CYCL bank
            "TRILIS177:METER:LAMBDA1",
            "TRILIS177:METER:LAMBDA2",
            "TRILIS177:METER:LAMBDA3",
            "TRILIS177:METER:LAMBDA4",
            "ILE2:LAS:RDPOWER",
            "NEUT:ASYM",
            "Keysight Voltage"
        ]
        
        self.cyclinfo = ["Not Used",
            "Bank 1 is for the Scan Type Code - it should always be 1",
            "Bank 2 is the Cycle Counter - it shows the number of ppg bunches measured - it should increment by ppg #",
            "Bank 3 is the Supercycle Counter - it shows the total number of dac steps - it should increment by 1",
            "Bank 4 is the Cycles/SC Counter- this is the number of ppg bunches - it should always show ppg #",
            "Bank 5 is the Sweep Counter - it shows which scan/sweep number it is on - it should step up by 1 every n x # dac steps - the final value is total number of scans",
            "Bank 6 is the Skipped Cycle Counter - this shows how many bunches have been skipped - it should increment by 1",
            "Bank 7 is the # of Cycles Histogrammed - this is how many ppg bunches have been measured - it should increment by ppg #",
            "Bank 8 is the Scan Increment Counter - this shows the dac increment of the current scan - it should increment from 0 to # dac increments for n x total dac scans",
            "Bank 9 is Scan Set Value - this should show the voltage output from the Galil unit for the dac increment (or EPICS setting)",
            "Bank 10 is the ADC[0] Input - this is the Galil input which directly reads the Galil voltage output - it should be in agreement with DAC[0] output",
            "Bank 11 is the ADC[1] Input - this is the Galil readback of the voltage from the /10000 divider (DO NOT x 10000 to get true experimental voltage - it is not calibrated - only use for diagnostic purposes)",
            "Bank 12 is the ADC[2] Input - it is connected to ground so should always read 0",
            "Bank 13 is the ADC[3] Input - it was connected to 6.5 V throughout the experiment (except when disconnected after HV issues) - to be used for diagnosis if needed",
            "Bank 14 is the Averaged value of ADC[0] Input - It is averaged over 10 samples so should be more precise",
            "Bank 15 is the Averaged value of ADC[1] Input - It is averaged over 10 samples so should be more precise",
            "Bank 16 is the Averaged value of ADC[2] Input - It is averaged over 10 samples so should be more precise",
            "Bank 17 is the Averaged value of ADC[3] Input - It is averaged over 10 samples so should be more precise",
            "Wavelength 1",
            "Wavelength 2",
            "Wavelength 3",
            "Wavelength 4",
            "Laser power",
            "Asymmetry",
            "Voltage from Keysight 34465A"
        ]
        
        self.extractnames = ["Ref Wavenumber:","Atomic Mass:","RFQ Voltage:","Laser Lock Wavenumber:","Kepco Calibration:","Harrison Voltage:","ToF Window Cut (bins):"]
        self.extractdefaults =[float(13923.5),"205.998670",float(19985),float(13917.455),float(50.3),float(0.0),int(65),int(90)]
        self.MCSnames = ["MCS0","MCS1","MCS2","MCS3"]
        self.switchnames = ["MCS0 RF (MHz):","MCS1 RF (MHz):","MCS2 RF (MHz):","MCS3 RF (MHz):"]
        self.switchdefaults = ["-100","-110","-100","-90"]
        self.mcs_labels = []
        self.rf_labels = []

        # Create Panel Titles
        label = self.createlabel("Import Run File Here:","black","white",0,0,1,"Helvetica",22)
        label = self.createlabel("Run Information:","black","white",1,0,2,"Helvetica",22)
        label = self.createlabel("MCS# Info:","black","white",3,0,2,"Helvetica",22)
        label = self.createlabel("Extract/Convert Variables:","black","white",5,0,2,"Helvetica",22)

        # Import File Panel Label and Button
        label = self.createlabel("No runxxxxx.mid file imported","white","red",0,5,1,"Helvetica",16)
        button0 = tkinter.Button( self, text=u"Import runxxxxx.mid file",command=self.importmid,cursor="hand1")
        button0.grid(column=0,row=6)

        # Create clear figure button
        button = tkinter.Button(self,text="Clear Fig",command=self.clearfig,font=("Helvetica", 14))
        button.grid(column=0,row=9)

        # List of Buttons for displaying/saving fig/file options
        for e in range(1,len(self.cyclnames)):
            dfbutton = self.createplotfigbutton(e,1,e,1,"Helvetica",12)
            #sfbutton = self.createsavefigbutton("Save Fig",e,2,e,1,"Helvetica",12)
            stbutton = self.createsavecyclbutton("Save Data",e,2,e,1,"Helvetica",12)
        
        # Create button for viewing variations in readback voltages
        self.var_button = tkinter.Button(self,text="View Variations in Scan Setting/Readback",command=self.plotvolts,font=("Helvetica", 12))
        self.var_button.grid(column=1,row=len(self.cyclnames),sticky='EW',columnspan=2)

        self.scan_v_counts_buttons = []

        # Panel for viewing MCS# Information
        for e in range(0,4):
            label = self.createlabel(self.MCSnames[e],"white","orange",3,(4*e)+1,2,"Helvetica",14)
            self.mcs_labels.append(label)
            button = self.createplotToFbutton("Plot ToF",e,3,(4*e)+2,1,"Helvetica",12)
            button = self.createsaveToFbutton("Save Data",e,4,(4*e)+2,1,"Helvetica",12)
            button = self.createsavehfsvoltbutton("Save Data",e,4,(4*e)+3,1,"Helvetica",12)
            button = self.createplothfsfreqbutton("Plot Frequency v Counts",e,3,(4*e)+4,1,"Helvetica",12)
            button = self.createsavehfsfreqbutton("Save Data",e,4,(4*e)+4,1,"Helvetica",12)

            button = self.createplothfsvoltbutton("Plot Scan Var v Counts",e,3,(4*e)+3,1,"Helvetica",12)
            self.scan_v_counts_buttons.append(button)
        
        # Panel for extracting/converting run data
        for e in range(0,len(self.extractnames)):
            label = self.createlabel(self.extractnames[e],"white","blue",5,(2*e)+1,2,"Helvetica",14)

        self.entryVariable0,self.entry0 = self.createentry(self.extractdefaults[0],5,2,2)
        self.entryVariable1,self.entry1 = self.createentry(self.extractdefaults[1],5,4,2)
        self.entryVariable2,self.entry2 = self.createentry(self.extractdefaults[2],5,6,2)
        self.entryVariable3,self.entry3 = self.createentry(self.extractdefaults[3],5,8,2)
        self.entryVariable4,self.entry4 = self.createentry(self.extractdefaults[4],5,10,2)
        self.entryVariable5,self.entry5 = self.createentry(self.extractdefaults[5],5,12,2)
        self.entryVariable6,self.entry6 = self.createentry(self.extractdefaults[6],5,14,1)
        self.entryVariable7,self.entry7 = self.createentry(self.extractdefaults[7],6,14,1)

        for e in range(0,4):
            label = self.createlabel(self.switchnames[e],"white","steel blue",5,e+15,1,"Helvetica",14)
            self.rf_labels.append(label)

        self.entryVariable8,self.entry8 = self.createentry(self.switchdefaults[0],6,15,1)
        self.entryVariable9,self.entry9 = self.createentry(self.switchdefaults[1],6,16,1)
        self.entryVariable10,self.entry10 = self.createentry(self.switchdefaults[2],6,17,1)
        self.entryVariable11,self.entry11 = self.createentry(self.switchdefaults[3],6,18,1)

    def createplotfigbutton(self,banknum,colnum,rownum,span,fonttype,fontsize):
        button = tkinter.Button(self,text=self.cyclnames[banknum],command=lambda: self.plotbank(banknum),font=(fonttype, fontsize))
        button.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)
        self.cycl_buttons[banknum] = button

    def createsavefigbutton(self,title,banknum,colnum,rownum,span,fonttype,fontsize):
        button = tkinter.Button(self,text=title,command=lambda: self.saveplot(banknum),font=(fonttype, fontsize))
        button.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)

    def createsavecyclbutton(self,title,banknum,colnum,rownum,span,fonttype,fontsize):
        button = tkinter.Button(self,text=title,command=lambda: self.savecycldata(banknum),font=(fonttype, fontsize))
        button.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)

    def createsaveToFbutton(self,title,MCSnum,colnum,rownum,span,fonttype,fontsize):
        button = tkinter.Button(self,text=title,command=lambda: self.saveToFdata(MCSnum),font=(fonttype, fontsize))
        button.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)

    def createplotToFbutton(self,title,MCSnum,colnum,rownum,span,fonttype,fontsize):
        button = tkinter.Button(self,text=title,command=lambda: self.plotToF(MCSnum),font=(fonttype, fontsize))
        button.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)

    def createsavehfsvoltbutton(self,title,MCSnum,colnum,rownum,span,fonttype,fontsize):
        button = tkinter.Button(self,text=title,command=lambda: self.savehfsvoltdata(MCSnum),font=(fonttype, fontsize))
        button.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)

    def createplothfsvoltbutton(self,title,MCSnum,colnum,rownum,span,fonttype,fontsize):
        button = tkinter.Button(self,text=title,command=lambda: self.plothfsvolt(MCSnum),font=(fonttype, fontsize))
        button.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)
        return button

    def createplothfsfreqbutton(self,title,MCSnum,colnum,rownum,span,fonttype,fontsize):
        button = tkinter.Button(self,text=title,command=lambda: self.plothfsfreq(MCSnum),font=(fonttype, fontsize))
        button.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)

    def createsavehfsfreqbutton(self,title,MCSnum,colnum,rownum,span,fonttype,fontsize):
        button = tkinter.Button(self,text=title,command=lambda: self.savehfsfreqdata(MCSnum),font=(fonttype, fontsize))
        button.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)

    def createlabel(self,labelname,fgc,bgc,colnum,rownum,span,fonttype,fontsize):
        label = tkinter.Label(self,text=labelname,anchor="center",fg=fgc,bg=bgc,font=(fonttype, fontsize))
        label.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)
        return label

    def createentry(self,setvalue,colnum,rownum,span):
        self.entryVariable = tkinter.StringVar()
        self.entry = tkinter.Entry(self,textvariable = self.entryVariable,justify="center")
        self.entry.grid(column=colnum,row=rownum,sticky='EW',columnspan=span)
        self.entryVariable.set(setvalue)
        return self.entryVariable,self.entry

    def plotbank(self,x):
        print( "Plotting " + self.cyclnames[x]+ ' Bank '+str(x))
        print( self.cyclinfo[x])
        plt.plot(self.cyclbanks[3],self.cyclbanks[x])
        plt.scatter(self.cyclbanks[3],self.cyclbanks[x])
        plt.xlabel('Supercycle counter',labelpad=15,fontsize=20)
        plt.ylabel(self.cyclnames[x]+ ' Bank '+str(x),labelpad=15,fontsize=20)
        plt.show()

    def plotvolts(self):
        # Average voltages of all sweeps for each dac step
        volts = np.average(self.dacvolts, axis=1)
        plt.plot(self.dacvolts)
        plt.plot(volts,c='k',lw=2,ls='--')
        if self.epics_scanned:
            plt.xlabel('EPICS Step',labelpad=15,fontsize=20)
            plt.ylabel('EPICS Readback',labelpad=15,fontsize=20)
        else:
            plt.xlabel('DAC Step',labelpad=15,fontsize=20)
            plt.ylabel('Readback Voltage (V)',labelpad=15,fontsize=20)
        plt.show()

    def clearfig(self):
        plt.clf()
        plt.show()

    def saveplot(self,x):
        print("Saving " + self.cyclnames[x]+ ' Bank '+str(x))
        plt.plot(self.cyclbanks[3],self.cyclbanks[x])
        plt.scatter(self.cyclbanks[3],self.cyclbanks[x])
        plt.xlabel('Supercycle counter',labelpad=15,fontsize=20)
        plt.ylabel(self.cyclnames[x]+ ' Bank '+str(x),labelpad=15,fontsize=20)
        savefile_path = tkinter.filedialog.asksaveasfilename()
        plt.savefig(savefile_path)
        plt.clf()
        print( "Saved " + self.cyclnames[x]+ ' Bank '+str(x))

    def savecycldata(self,x):
        print( "Saving " + self.cyclnames[x]+ ' Bank '+str(x) + ".txt file...")
        savefile_path = tkinter.filedialog.asksaveasfilename()
        output = open(savefile_path,'w')
        for ii in range(len(self.cyclbanks[3])):
            output.write('%f %f \n' % (self.cyclbanks[3][ii],self.cyclbanks[x][ii]))
        output.close
        print( "Saved " + self.cyclnames[x]+ ' Bank '+str(x) + ".txt file...")

    def saveToFdata(self,x):
        print( "Saving ToF data for MCS" + str(x) + " into .txt file...")
        savefile_path = tkinter.filedialog.asksaveasfilename()
        output = open(savefile_path,'w')
        for ii in range(len(self.alltimes[x])):
            output.write('%f %f \n' % (ii,self.alltimes[x][ii]))
        output.close
        print( "Saved ToF data for MCS" + str(x) + " .txt file...")

    def plotToF(self,x):
        print( "Plotting Tof spectrum for MCS" + str(x))
        plt.plot(self.alltimes[x])
        plt.xlabel('ToF (us)',labelpad=15,fontsize=20)
        plt.ylabel('Counts',labelpad=15,fontsize=20)
        plt.show()

    def plothfsvolt(self,x):
        self.sortvolt(x)
        print( "Plotting Photon Counts vs scanned variable for MCS" + str(x))
        plt.plot(self.finalsets,self.MCScounts)
        label = "EPICS setting" if self.epics_scanned else "Voltage (V)"
        plt.xlabel(label,labelpad=15,fontsize=20)
        plt.ylabel('Counts',labelpad=15,fontsize=20)
        plt.show()

    def sortvolt(self,x):
        self.MCScounts = deepcopy(self.counts)
        # Remove all the tof information outside the time cut and add to counts
        for i in range(0,self.dacsteps):
            self.MCScounts[i] =self.dactofs[x][i][int(self.entryVariable6.get()):int(self.entryVariable7.get())+1]
        # Sum tof counts of all sweeps in each dac step
        self.MCScounts = np.array(self.MCScounts)
        self.MCScounts = np.sum(self.MCScounts,axis=1)
        self.finalsets = np.average(self.dacsets, axis=1)

    def savehfsvoltdata(self,x):
        self.sortvolt(x)
        print( "Saving scanned variable v counts data for MCS" + str(x) + " into .txt file...")
        savefile_path = tkinter.filedialog.asksaveasfilename()
        output = open(savefile_path,'w')
        for ii in range(len(self.finalsets)):
            output.write('%f %f \n' % (self.finalsets[ii],self.MCScounts[ii]))
        output.close
        print( "Saved scanned variable v counts data for MCS" + str(x) + " .txt file...")

    def plothfsfreq(self,x):
        self.sortfreq(x)
        plt.plot(self.freqs,self.MCScounts)
        plt.xlabel('Relative Frequency (MHz)',labelpad=15,fontsize=20)
        plt.ylabel('Counts',labelpad=15,fontsize=20)
        plt.show()

    def sortfreq(self,x):
        self.MCScounts = deepcopy(self.counts)
        self.aoms = [float(self.entryVariable8.get()),float(self.entryVariable9.get()),float(self.entryVariable10.get()),float(self.entryVariable11.get())]
        print( "Plotting Photon Counts vs frequency for MCS" + str(x))
        # Remove all the tof information outside the time cut and add to counts
        for i in range(0,self.dacsteps):
            self.MCScounts[i] = self.dactofs[x][i][int(self.entryVariable6.get()):int(self.entryVariable7.get())+1]
        # Sum tof counts of all sweeps in each dac step
        self.MCScounts = np.array(self.MCScounts)
        self.MCScounts = np.sum(self.MCScounts,axis=1)
        self.finalsets = np.average(self.dacsets, axis=1)
        # Convert into frequencies
        # expvolts = (dacvolts * kepco calibration) + harrison voltage
        expvolts = (self.finalsets*float(self.entryVariable4.get())) + float(self.entryVariable5.get())
        # alpha = (RFQ - expvolt)/mass of isotope
        alpha = (float(self.entryVariable2.get()) - expvolts)/(float(self.entryVariable1.get())*931494000.0)
        wavenumber = float(self.entryVariable3.get()) + (2.0/29979.2458)*self.aoms[x]
        wavenumber2 = wavenumber * (1. + alpha + np.sqrt((2*alpha) + (alpha**2)))
        self.freqs = 29979.2458*wavenumber2 - 29979.2458*float(self.entryVariable0.get())

    def savehfsfreqdata(self,x):
        self.sortfreq(x)
        print( "Saving frequency v counts data for MCS" + str(x) + " into .txt file...")
        savefile_path = tkinter.filedialog.asksaveasfilename()
        output = open(savefile_path,'w')
        for ii in range(len(self.freqs)):
            output.write('%f %f \n' % (self.freqs[ii],self.MCScounts[ii]))
        output.close
        print( "Saved frequency v counts data for MCS" + str(x) + " .txt file...")

    def rename_mcs_rf_labels(self, idx, name):
        self.mcs_labels[idx]['text'] = name
        short = name.replace("Frequencies", "Freqs").replace("Frequency", "Freq")
        self.rf_labels[idx]['text'] = "%s RF (MHz)" % short

    def rename_scan_labels(self, short, long):
        self.var_button['text'] = "View Variations in %s" % long

        self.cyclnames[9] = short
        self.cycl_buttons[9]['text'] = self.cyclnames[9]

        for button in self.scan_v_counts_buttons:
            button['text'] = "Plot %s v Counts" % short

    def importmid(self):
        try:
            args = {"filetypes": [("Midas files", ".mid .gz .lz4")]}

            if self.last_open_dir is not None:
                args["initialdir"] = self.last_open_dir
            elif os.path.exists("/data1/pol/data/current"):
                args["initialdir"] = "/data1/pol/data/current"

            file_path = tkinter.filedialog.askopenfilename(**args)

            if file_path == "" or file_path is None:
                return

            self.last_open_dir = os.path.dirname(file_path)
            print("Importing %s" % file_path)
            label = self.createlabel("Importing %s" % os.path.basename(file_path),"black","yellow",0,5,1,"Helvetica",16)
            self.update()
            
            polfile = pol_data.PolFile(file_path)
            
            runinfo = polfile.run_info
            mcs_info = pol_data.get_histo_info(runinfo)

            self.epics_scanned = False if runinfo is None else runinfo.is_epics_scanned()
            
            self.dacsteps = 0
            self.cyclbanks = []
            self.dacsets = []
            self.dacvolts = []
            self.counts = []
            self.mcsorder = []
            self.dacsweeptofs = []

            for idx, (bank_name, info) in enumerate(mcs_info.items()):
                if idx <= 4:
                    # Only 4 labels in GUI
                    self.rename_mcs_rf_labels(idx, info["human_name"])
                self.mcsorder.append(info["scaler_chan"])
                self.dacsweeptofs.append([])

            self.num_mcs = len(self.mcsorder)

            for idx in range(self.num_mcs, 4):
                self.rename_mcs_rf_labels(idx, "N/A")

            if self.epics_scanned:
                self.rename_scan_labels("EPICS", "EPICS Readback")
            else:
                self.rename_scan_labels("DAC Volt", "Readback Voltages")

            self.cyclbanks = []

            for i in range(len(self.cyclnames)):
                self.cyclbanks.append([])

            for ev in polfile:
                for i, info in enumerate(ev.cycle_info):
                    # Old code uses 1-based indexing....
                    self.cyclbanks[i+1].append(info)

                dacstep = int(ev.hisi_info[4])
                self.dacsteps = max(self.dacsteps, dacstep)
                
                while len(self.dacsets) <= dacstep:
                    self.dacsets.append([])
                    self.dacvolts.append([])
                    self.counts.append([])

                    for i in range(self.num_mcs):
                        self.dacsweeptofs[i].append([])

                self.dacsets[dacstep].append(ev.hisi_info[2])
                self.dacvolts[dacstep].append(ev.hisi_info[3])

                for idx, chan in enumerate(self.mcsorder):
                    self.dacsweeptofs[idx][dacstep].append(ev.hists[chan])

                self.cyclbanks[18].append(ev.epics_readback.get("TRILIS177:METER:LAMBDA1", 0))
                self.cyclbanks[19].append(ev.epics_readback.get("TRILIS177:METER:LAMBDA2", 0))
                self.cyclbanks[20].append(ev.epics_readback.get("TRILIS177:METER:LAMBDA3", 0))
                self.cyclbanks[21].append(ev.epics_readback.get("TRILIS177:METER:LAMBDA4", 0))
                self.cyclbanks[22].append(ev.epics_readback.get("ILE2:LAS:RDPOWER", 0))
                self.cyclbanks[23].append(ev.epics_readback.get("NEUT:ASYM", 0))
                self.cyclbanks[24].append(ev.keysight_voltage)

            self.dacsteps += 1

            # Find number of full sweeps
            if len(self.dacsweeptofs[0]) == 0:
                raise ValueError("No MCS data in this file!")

            sweepnum = len(self.dacsweeptofs[0][-1])

            # Remove information for any additional sweeps
            for i in range(0,len(self.dacsweeptofs[0])):
                if len(self.dacsweeptofs[0][i]) > sweepnum:
                    for ii in range(0,self.num_mcs):
                        self.dacsweeptofs[ii][i].pop()
                    self.dacvolts[i].pop()
                    self.dacsets[i].pop()

            # Turn lists into arrays
            self.dacvolts = np.array(self.dacvolts)
            self.dacsweeptofs = np.array(self.dacsweeptofs)

            # For each DAC step sum the time info over all sweeps
            self.dactofs = [ [] for i in range(0,self.num_mcs)]
            for i in range(0,len(self.dacsweeptofs[0])):
                for ii in range(0,self.num_mcs): 
                    self.dactofs[ii].append(sum(self.dacsweeptofs[ii][i]))

            # Sum all ToF count info to get total counts in each time slice
            self.alltimes = [ [] for i in range(0,self.num_mcs)]
            for i in range(0,self.num_mcs):
                self.alltimes[i] = sum(self.dactofs[i])

            # Set ToF cut range to full bin range initially
            self.entryVariable6.set(0)
            self.entryVariable7.set(len(self.dacsweeptofs[0][0][0]))
            
            label = self.createlabel("Imported %s" % os.path.basename(file_path),"white","green",0,5,1,"Helvetica",16)
        except Exception:
            traceback.print_exc()
            label = self.createlabel("Import failed!","white","red",0,5,1,"Helvetica",16)


if __name__ == "__main__":
    app = simpleapp_tk(None)
    app.title('Run Viewer')
    app.mainloop()


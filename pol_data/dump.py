"""
Tool that will print POL data to screen.

If an argument is provided, we open that midas file.
If no argument is provided, we connect to the live midas experiment.
"""

try:
    import midas
    import midas.client
    got_client = True
except (ImportError, SyntaxError, EnvironmentError):
    import pol_data.midas_pol as midas
    got_client = False
    
import pol_data
import argparse

def handle_event(pol_event, as_csv):
    if as_csv:
        # Event info
        items = ["%.3f" % pol_event.event_time, "%d" % pol_event.event_number]
        # Scan values
        items.extend("%f" % v for v in pol_event.get_all_scanned_vars(None).values())
        # Histo sums
        items.extend("%d" % s for s in pol_event.sums_dict.values())
        # Laser readback
        items.extend("%d" % s for s in pol_event.epics_readback.values())
        # Keysight voltage
        items.append("%f" % pol_event.keysight_voltage)
        # Print
        print(",".join(items))
    else:
        pol_event.dump_to_screen()

def print_csv_header(pol_event, run_info):
    # Event info
    items = ["Event time (s)", "Event number"]
    # Scan values
    items.extend("%s setting" % k for k in pol_event.get_all_scanned_vars(None).keys())
    # Histo sums
    items.extend("%s sum" % pol_data.scaler_chan_to_human_name(run_info, k) for k in pol_event.sums_dict.keys())
    # Laser readback
    items.extend("%s" % s for s in pol_event.epics_readback.keys())
    # Keysight voltage
    items.append("Keysight voltage (V)")
    # Print
    print(",".join(items))

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Dump POL data to screen')
    file_help = "Midas file must be provided"
    file_nargs = 1
    if got_client:
        file_help = "If midas file not provided, connects to live experiment"
        file_nargs = '?'
    parser.add_argument('filename', metavar="midas_file", nargs=file_nargs, help=file_help)
    parser.add_argument('--as-csv', action='store_true', help="Dump summary data as CSV")
    args = parser.parse_args()

    printed_csv_header = False

    if args.filename is not None:
        # Print from file
        file_path = args.filename[0] if file_nargs == 1 else args.filename

        reader = pol_data.PolFile(file_path)

        if not args.as_csv:
            if reader.run_info is not None:
                reader.run_info.dump_to_screen()
    
        for pol_event in reader:
            if args.as_csv and not printed_csv_header:
                print_csv_header(pol_event, reader.run_info)
                printed_csv_header = True

            handle_event(pol_event, args.as_csv)
    else:
        # Print live events
        if not got_client:
            print("Midas must be installed (and the full midas python packages available) for live events to be dumped")
            exit(0)
        
        client = midas.client.MidasClient("poldump")
        buffer_handle = client.open_event_buffer("SYSTEM")
        client.register_event_request(buffer_handle, sampling_type=midas.GET_ALL)
        
        curr_run = None
        
        while True:
            # Reload run-level settings if needed
            new_run = client.odb_get("/Runinfo/Run number")
            
            if new_run != curr_run:
                run_info = pol_data.PolRunInfo(client.odb_get("/"))
                curr_run = new_run
                printed_csv_header = False
            
            # Handle any events
            midas_event = client.receive_event(buffer_handle, True)
            
            if midas_event:
                try:
                    pol_event = pol_data.PolEvent(midas_event, run_info)
                except ValueError:
                    continue
                
                if args.as_csv and not printed_csv_header:
                    print_csv_header(pol_event, run_info)
                    printed_csv_header = True

                handle_event(pol_event, args.as_csv)
                
            client.communicate(100)
            

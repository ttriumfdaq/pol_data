import json

# Data types                                   min      max    
TID_BYTE     =  1       # unsigned byte         0       255
TID_UINT8    =  TID_BYTE    
TID_SBYTE    =  2       # signed byte         -128      127    
TID_INT8     =  TID_SBYTE    
TID_CHAR     =  3       # single character      0       255    
TID_WORD     =  4       # two bytes             0      65535   
TID_UINT16   =  TID_WORD    
TID_SHORT    =  5       # signed word        -32768    32767   
TID_INT16    =  TID_SHORT
TID_DWORD    =  6       # four bytes            0      2^32-1  
TID_UINT32   =  TID_DWORD
TID_INT      =  7       # signed dword        -2^31    2^31-1  
TID_INT32    =  TID_INT
TID_BOOL     =  8       # four bytes bool       0        1     
TID_FLOAT    =  9       # 4 Byte float format                  
TID_FLOAT32  = TID_FLOAT
TID_DOUBLE   = 10       # 8 Byte float format                  
TID_FLOAT64  = TID_DOUBLE
TID_BITFIELD = 11       # 32 Bits Bitfield      0     111... (32) 
TID_STRING   = 12       # null-terminated string               
TID_ARRAY    = 13       # array with unknown contents          
TID_STRUCT   = 14       # structure with fixed length          
TID_KEY      = 15       # key in online database               
TID_LINK     = 16       # link in online database    
TID_INT64    = 17       # 8 bytes int          -2^63   2^63-1  */
TID_QWORD    = 18       # 8 bytes unsigned int  0      2^64-1  */
TID_UINT64   = TID_QWORD

# Number of bytes each midas data type requires
tid_sizes = {  TID_BYTE: 1, 
               TID_SBYTE: 1,
               TID_CHAR: 1, 
               TID_WORD: 2,
               TID_SHORT: 2,
               TID_DWORD: 4,
               TID_INT: 4,
               TID_BOOL: 4,
               TID_FLOAT: 4,
               TID_DOUBLE: 8,
               TID_BITFIELD: 4,
               TID_STRING: None,
               TID_ARRAY: None,
               TID_STRUCT: None,
               TID_KEY: None,
               TID_LINK: None,
               TID_INT64: 8,
               TID_QWORD: 8
            }

# How to unpack each midas data type with python's struct module
tid_unpack_formats = {  TID_BYTE: 'B', # C char / python int
                        TID_SBYTE: 'b', # C signed char / python int
                        TID_CHAR: 'B', # C char / we'll make a python string 
                        TID_WORD: 'H', # C unsigned short / python int
                        TID_SHORT: 'h', # C signed short / python int
                        TID_DWORD: 'I', # C unsigned int / python int
                        TID_INT: 'i', # C signed int / python int
                        TID_BOOL: 'I', # C unsigned int / we'll make a list of python bools
                        TID_FLOAT: 'f', # C float / python float
                        TID_DOUBLE: 'd', # C double / python double
                        TID_BITFIELD: 'I', # C unsigned int / python int 
                        TID_STRING: None, # We just give raw bytes
                        TID_ARRAY: None, # We just give raw bytes
                        TID_STRUCT: None, # We just give raw bytes
                        TID_KEY: None, # We just give raw bytes
                        TID_LINK: None, # We just give raw bytes
                        TID_QWORD: 'Q', # C unsigned long long / python int
                        TID_INT64: 'q', # C signed long long / python int
                    }

# Friendly name of each midas data type
tid_texts = {  TID_BYTE: "Unsigned Byte", 
               TID_SBYTE: "Signed Byte",
               TID_CHAR: "Char", 
               TID_WORD: "Unsigned Word",
               TID_SHORT: "Signed Word",
               TID_DWORD: "Unsigned Integer",
               TID_INT: "Signed Integer",
               TID_BOOL: "Boolean",
               TID_FLOAT: "Float",
               TID_DOUBLE: "Double",
               TID_BITFIELD: "Bitfield",
               TID_STRING: "String",
               TID_ARRAY: "Array",
               TID_STRUCT: "Struct",
               TID_KEY: "Key",
               TID_LINK: "Link",
               TID_QWORD: "Unsigned 64-bit Integer",
               TID_INT64: "Signed 64-bit Integer"
            }

# Read in little-endian by default
endian_format_flag = "<"

def safe_to_json(input_str, use_ordered_dict=False):
    """
    Convert input_str to a json structure, with arguments that will catch
    bad bytes.

    Args:

    * input_str (str)
    * as_ordered_dict (bool) - Whether to preserve the order of keys in the
        JSON document by using OrderedDict instead of dict.

    Returns:
        dict
    """
    decoded = input_str.decode("utf-8", "ignore")
    if use_ordered_dict:
        return json.loads(decoded, strict=False, object_pairs_hook=collections.OrderedDict)
    else:
        return json.loads(decoded, strict=False)
from setuptools import setup

setup(name='pol_data',
      version='0.1',
      description='Python tools for reading POL midas files',
      author='Ben Smith',
      author_email='bsmith@triumf.ca',
      packages=['pol_data'])
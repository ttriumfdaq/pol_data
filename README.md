# Python tools for reading Polarizer data

## Installation

    git clone https://bitbucket.org/ttriumfdaq/pol_data.git
    cd pol_data/pol_data

    # If you use pip
    pip install -e .

    # If you use linux but don't use pip
    export PYTHONPATH=$PYTHONPATH:`pwd`

    # If you use Windows, TBD...

Required dependencies for the GUI are tkinter and numpy. Installation instructions to follow.

## Features

* `gui.py` contains a graphical interface for plotting data and exporting as text files
* `dump.py` will print a summary of events to screen (mostly useful for debugging)
* `midas_pol/*.py` contains tools for parsing midas files
* `__init__.py` converts POL midas files into python objects

You can read midas files taken with the old DAQ and the new one (from 2022). The new DAQ has more features, but the files are backwards-compatible.

## Changes to the GUI compared to Tom's original version

The GUI is based on code written by Tom Proctor. His version had several features specific to the beamtime he was analyzing, and required midas files to be converted to text files before being loaded.

The GUI in this repository:

* can read midas files directly
* will parse all the data in each ToF histogram (previously limited to the first 112 bins)
* does not set a default ToF range cut (previously only counted data in bins 60-95 by default)
* does not hard-code the meaning of each scaler channel (e.g. input 21 always used to be called "All Frequencies"; now operators can define the meaning/name before the start of each run)

## Reading compressed files

You can always read `.mid` files and `.mid.gz` files. To read `.mid.lz4` files you may need to run `pip install lz4` or similar.